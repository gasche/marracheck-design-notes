Retraite Mirage
Armaël Guéneau


Armaël: même dans le setting simple où on suppose que toutes les
descriptions de paquet sont correctes, on ne peut juste merger
trivialement les cones de chaque dépendance directe, parce qu'on ne
peut pas installer deux versions différentes d'un même
paquet.

  Typiquement, si A dépend de B et C, B dépend de Toto.1, C dépend de
  Toto.1 ou Toto.2, si tu commences avec B qui dépend de Toto.1, et
  C avec Toto.2, tu ne peux pas merger.
  
  Du coup, j'ai une idée, Louis dit que ça marche. Je merge les cônes de
  manière débile, j'ai une solution incompatible, mais je demande au
  solveur d'OPAM de minimiser le nombre de paquets supprimés -- et donc
  le nombre de recompilations qu'on a à faire.
  
  Gabriel: appeler le solveur d'OPAM, ça prend du temps, on peut faire
    mieux si on contrôle l'incrémentalité -- c'est ce que fait Damien.
  
  Armaël: mais si tu fais une résolution sous-optimale, ensuite tu
    recompiles plus de truc, donc on y gagne en schedulant de manière
    maline. À tester.

  Armaël: mais c'est vrai que cette solution est plutôt dynamique, on
    peut aussi essayer de faire un scheduling tout-statique. Dans ce
    cas le choix est de re-scheduler C et de compilater avec Toto.1
    à la place.

Gabriel: Mais si tu essaies de tout scheduler statiquement, pourquoi
  ne pas directement demander au solveur des couvertures maximales, et
  en tirer les cônes ? Un ensemble de plans d'install qui, à eux tous,
  couvrent tous les paquets.

  Louis raconte qu'il a fait ça, la façon de faire c'est : tu essaies
    de tout installer à la fois, tu trouves une solution sans certains
    paquets, et ensuites tu recommences en forçant une solution qui
    contient l'un des paquets manquants. Rinse and repeat, ça termine
    en 4/5 itérations en pratique d'après Louis.

  Armaël: est-ce que ça minimise le nombre de recompilations ?

  Gabriel: en soit ça ne garantit par le partage entre les éléments
    différents de la couverture, mais on peut aussi essayer d'améliorer
    ça.

  Armaël: bien sûr avec mon idée: au lieu de partir de 0 pour le
    prochain élément de la couverture, tu pars de la solution
    précédente et tu lui demandes de minimiser le nombre de
    déinstallations.

  Gabriel: il faudrait comparer avec ce que fais Damien du point de
    vue du sat-solving.

Armaël: si les descriptions de paquets sont correctes, on suppose
  qu'il n'y a pas d'interactions non spécifiées entre les paquets, donc
  les recettes sont stables par extension. (On a le droit de prendre
  l'effet d'un paquet et le rejouer tel-quel dans un monde plus grand.)


Gabriel: donc on a une idée qui marche si les descriptions de build
  sont correctes. Comment on s'étend au cas incorrect ?

  1. Faire du scheduling partiellement dynamique, quand on tombe sur
    une erreur. (Comment ?)

  2. Enregistrer les trucs qui marchent et qui foirent pour refaire un
    scheduling statique plus fin la fois d'après. (Question bonus:
    peut-on transformer cette information en des patches sur le
    opam-repository automatiquement ?)
 
Gabriel: premier trick, si on a une erreur dynamique, en fait on
  refait le scheduling statique à partir de 0 en interdisant le
  paquet+version qui a foiré (TODO: mais peut-être il faut interdire
  un autre aspect du build ?), et on prie pour que la solution soit
  assez proche pour que presque tout soit réutilisé / mémoisé du build
  qui vient d'échouer.

Armaël: deuxième trick, au lieu de repartir de zéro complètement,
  à nouveau on demande des solutions qui minimisent les
  désinstallations à partir de l'état maximal correct courant.

Armaël: il y a aussi un problème, comme on peut avoir des interactions
  non-déclarées entre paquets, on n'a plus le droit de fusionner des
  cônes.

Gabriel: je pense qu'on peut s'autoriser à fusionner quand même,
  quitte à perdre un peu en correction. Si on trouve un plan qui
  marche quand même, il pourrait en théorie échouer dans la vraie vie,
  mais on a bien testé le compilateur. Si un plan qui aurait dû
  marcher échoue, notre outil trouve une autre solution.

  La question importante pour moi c'est : est-ce que ce serait
  possible de détecter les dépendances manquantes, pour savoir si on
  est dans la partie garantie-correcte ou si on s'est écarté de la
  partie correcte ? Si on pouvait le flagguer dynamiquement, on
  pourrait même essayer de détecter et corriger dans un second temps.

Armaël: on est pas mal conservatifs parce qu'on a buildé les trucs
  séparéments. Si tu merges C1 et C2, on aurait planté s'il y avait
  une dépendance non spécifiée de C1 vers C2. Par contre si A dépend
  d'un des paquets qui sont dans C1 ou C2 implicitement, tu vas
  l'importer alors que tu l'auras spécifié. Tu peux au moins record "A
  pourrait au moins dépendre de ces paquets en plus"

Gabriel: on pourrait aussi faire aussi de la subsumption, si on
  a besoin d'un set de paquet et qu'on a déjà buildé un cône qui est
  un superset, alors on utilise ce cône. Est-ce qu'on essaie de
  retirer les paquets en trop dans ce cas, ou on les garde ?

  Retirer:
  + permet de détecter des dépendances non déclarées.
  - risque de changement de comportement des paquets nécessaires,
    installés en présence des paquets supplémentaires (en cas de
    dépendance non déclarées), mais qui tournent maintenant en leur
    absence

    Par exemple si une dépendance utile U avait une dépendance
    non-déclarée sur un paquet visiblement-inutile I du cône plus
    grand, je retire I mais je garde U, et U a été installé en
    présence de I mais est utilisé sans. Problème de correction.

  Est-ce qu'on peut se convaincre que le (-) n'arrive jamais, parce
  que par induction on builde chaque paquet dans son cône minimal ?
  (Donc U ne peut pas avoir vu I pendant son build.)

  Invariant qu'on préserve: chaque paquet est installé dans son cône
  minimal.



# Kate

Gabriel: l'outil de Fabrice, son trick principal c'est de voir
  l'installation de paquets comme un diff sur le .opam, il met le diff
  dans un .tar.gz et il peut replay une installation
  rapidement. Ensuite il essaie d'installer chaque paquet
  indépendamment, mais pour chaque dépendance il regarde s'il l'a déjà
  buildée, et dans ce cas il unpacke le .tar.gz directement.

  (Et il a une sortie web utilisable.)

Gabriel: le but c'est de marcher sur un laptop en une journée.

Kate: avec Dune, maintenant c'est très rapide de builder plein de
  paquets. opam update me prend 15mn (-j4).

Armaël: et être incrémental par rapport à une mise à jour de
  opam-repository.

  Kate, Gabriel sont d'accord.


Gabriel: l'outil de Damien, il est beaucopu plus malin sur
  l'incrémentalité, mais il est très conservatif: il suppose que les
  metadonnées du opam-repository sont globalement fuasses, il veut
  trouver quand même des chemins qui marchent pour pouvoir tester le
  compilateur le plus loin possible. Il utilise son propre SAT-solveur
  pour trouver rapidement des chemins d'install, et quand un truc
  échoue il met à jour dynamiquement ses plans de build pour
  contourner le problème. (C'est malin et compliqué.)

  Un deuxième truc malin qu'il fait c'est l'utilisation de git: au
  lieu de .tar.gz par paquets comme Fabrice, il stocke son .opam dans
  un dépôt git, et il fait "git checkout" pour retourner à un état
  précédent qu'il veut étendre avec plus de paquets. Dans la majorité
  des cas, beaucoup de trucs sont déjà dans le workdir et c'est
  quasi-instantané.

  Armaël: Par contre il y a une grosse limitation, c'est qu'il n'est
  pas du tout parallélisable, à cause de son workflow git. Il
  y a aussi des trucs dans les hypothèses qu'il fait, par exemple il
  se souvient de l'ordre dans lequel les paquets ont été installés et
  il refuse de fusionner deux solutions qui ont pris des ordres
  différents -- on croit. Donc moins de partage à cause de l'approche
  conservatrice vis-à-vis de la qualité des métadonnées OPAM.

  Armaël: un troisième truc malin qu'il fait , quand il y a une
  recette qui était censée fonctionner et qui ne marche pas, il a une
  heuristique sur comment la corriger, comment parcourir d'autres
  solutions, notamment il privilégie les solutions qui changent des
  dépendances directes -- si tu dis juste "donne-moi une autre
  solution", le solveur va juste changer la version d'ocamlfind et ça
  a peut de chances de résoudre le problème.

Kate: mon outil fait toutes les versions de OCaml de 4.02 à 4.07, tous
  les paquets, en deux jours. Aucun partage, une nouvelle image Docker
  par paquet (avec les depexts installées pour tout le monde). 32
  cœurs et 200Gio de RAM.

  Question: est-ce que le parallélisme au niveau des solutions est
    important, ou on peut juste paralléliser le build de chaque paquet
    et espérer utiliser tous nos cœurs ?

    Kate pourrait tester son outil sans parallélisme toplevel, juste
    le parallélisme du build de chaque paquet, et voir à quel point
    c'est plus lent.

    Kate: je dirais 2/3 fois plus lent.

    Gabriel: ce serait cool de tester, pour une version de OCaml
    précise.

    Kate: le principal soucis ce serait les paquets qui compilent des
    trucs Coq. Si tu compiles Why3, tu vas compiler des bibliothèques
    Coq, ça se parallélise très mal et ça prend beaucoup de CPU.

Kate: attention, si tu veux merger deux cônes de dépendances C1 et C2,
  tu vas avoir un problème de reproducibilité. Si C1 et C2 contiennent
  des paquets en commun, ils peuvent quand même les avoir buildés avec
  des .cmi différents par exemple, et tu vas avoir des clash.

  Armaël: est-ce possible que C1 et C2 ont dupliqué des dépendances ?
    S'ils dépendent tous les deux de Toto, ils vont partir du même
    checkout de Toto et ses dépendances, donc pas de problème.

  Gabriel: on fait du hash-consing de paquets.

Kate: je pense que tu ne veux pas prendre tout opam-repository, mais
  plutôt un subset des dernières versions de chaque paquet.

Armaël: après si on a des plans qui font toutes les versions de tous
  les paquets, on peut toujours ordonner ces plans et partir de ceux
  avec les versions les plus récentes en premier.

Kate: je suis pas convaincue de l'idée de faire chaque chose dans un
  build minimal.

Gabriel: idéalement si on avait un truc qui fait les deux, tester le
  compilateur et améliorer la santé du opam-repository, c'est encore
  mieux.

Kate: mais si tu veux être le plus rapide possible, ça me paraît
  intéressant d'installer le plus de paquets possibles d'un coup, sans
  partir d'un set plus petit.

Armaël: ça peut être une option, -minimal-sets
  or -non-minimal-sets. (Quand il n'est pas activé, la subsumption est
  juste l'identité, pas besoin de supprimer les paquets en trop.)

Armaël: avec le truc minimal, tu ne vas jamais trouver les cas où un
  paquet non-déclaré fait marcher ton build. Il y a des paquets qu'on
  n'arrivera pas à installer car on n'arrivera jamais à deviner tout
  seul où il faut installer le paquet foo. Avec la technique de Kate,
  par chance le paquet foo pourrait être là. Donc c'est mieux sans
  l'option -minimal-set, en effet.

Conclusion: deux options, -minimal-sets et -non-minimal-sets. Ok.

Kate: tu ne peux pas faire -minimal-sets quand tu as des depexts, tu
  ne peux pas désinstaller tes depexts.

Problème de conflits entre depext ? On ignore ça pour l'instant.

Armaël: il faut demander à Louis si on peut vraiment faire ça.

Louis: dans opam2 il y a un système de caches binaires de
  paquets. Pour avoir plein de partage. Si tu recompiles plusieurs
  fois le même paquet avec les mêmes versions des dépendances, il va
  juste récupérer ce qu'il a dans le cache. opam2 définit un "build
  id" pour les paquets, qui les build-id de toutes les dépendances
  plus la version précise de ton paquet. Ça te permet tout à fait de
  servir de clef dans un système de stockage binaire. Si tu configures
  ces hook-là, déjà tu optimises au maximum. Après il faut calculer
  ton plan de bataille pour tout builder.

Louis: si le cache est infiniment rapide, tu peux faire "opam install"
  sur chaque paquet.

Kate: je suis en train de penser à un truc.

Gabriel: comment marche le cache ? C'est plus rapide que les .tar.gz
de diff de Fabrice ?

  Louis: opam2 il utilise les dates des paquets, et il connaît tous
  les fichiers créés par chaque paquet. On copie ça dans un dossier de
  cache, avec des script shell, j'ai un hook après l'install de chaque
  paquet où il crée un dossier avec le build-id du paquet, il copie le
  sous-arbre.

Armaël: il reste à déterminer l'ordre dans lequel tu envoies les
  instructions de build. Quand tu es au sein d'une couverture, tous
  les ordres sont compatibles. Mais entre deux compatibles ça change.

Louis: il faut faire ta couverture pour minimiser le nombre total de
  compilations, c'est-à-dire le nombre de choix différents de
  dépendances de chaque paquet. (Je ne compte pas les depopts.)

Gabriel: j'ai l'impression qu'on n'est pas complètement au clair sur
  comment construire la couverture de l'espace de paquets. Les
  propositions dépendent beaucoup de ce que savent faire les solveurs
  OPAM, ça a l'air un peu fragile.



---


Armaël: pour la génération du planning, on a un planning initial et
ensuite, pour tous les paquets qui ne sont pas dans le planning, on
veut dériver un nouveau planning, à partir des modifications minimales
du planning qu'on a. Mais donc on peut étendre l'un des plannings
précédents, pas juste le dernier !

Si on veut être optimal, il faudrait dire, pour tous les plannings
existants, essayer de l'étendre avec un autre planning. Mais ça peut
être cher... Faire le scheduling, ça peut prendre beaucoup de temps.

La deuxième remarque, j'ai repensé au fait que oui, du coup, si tu as
une recette qui était censée fonctionner, je ne vois pas de meilleur
moyen que de refaire tout le scheduling. Qu'est-ce qu'on peut garder ?
Ce n'est pas clair. C'est un problème si c'est cher de refaire le
scheduling.

Gabriel: on pourrait blacklister le truc et ses dépendances directes.

Armaël: oui, c'est ce que fait Damien.

Louis: vous voulez peut-être essayer -best-effort


---

Beaucoup de travail. On génère le premier élément de la couverture
avec Z3, en 5 secondes.
