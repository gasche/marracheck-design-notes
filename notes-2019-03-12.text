Hier soir Armaël et Gabriel ont travaillé sur la découpe des fonctions
de OpamSolution.ml, qui calculent un graphe d'action en appelant le
solver et appliquent les actions d'un graphe d'actions, pour pouvoir
les utiliser de façon plus flexible depuis opam-libs. (En récupérant
toutes les infos sur les échecs/succès/aborts.)

----

Armaël: je propose qu'on esquisse le design et les features de l'outil
version 1 un peu en détail, et après on code.

Gabriel: je pense que c'est important que d'autres gens que nous
puissent utiliser l'outil facilement.

Le 9 j'avais proposé:

> 1. Calculer une couverture pour une version de OCaml donnée.
> 2. Installer les depexts pour la couverture.
> 3. Builder les éléments de la couverture, séquentiellement.
> 4. Nettoie le cache à partir des informations d'usages qu'on
>    a stockées quelque part -- notre script (3) quand il peuple et
>    utilise le cache.

Cette liste ne prend pas en compte la mise à jour de la couverture en
cas d'échec. On veut aussi pouvoir distribuer aux gens notre propre
couverture pour éviter les échecs dès le début. Une "known-good"
couverture pour un état récent de opam-repository.

Armaël: je me dis que l'outil, ça devrait être un binaire
self-contained unique. Il peut être dans ton path n'importe où. Tu le
lances en lui donnant un dossier, forcément, dans ce dossier il écrit
ses trucs.

Gabriel: y compris une OPAMROOT dans le dossier, indépendant du monde
extérieur ?

Armaël: ouais, ouais.

Armaël: dans ce dossier en argument, il crée une OPAMROOT avec les
bons paramètres setup: (1) le cache binaire, (2) le bon repository.

Par défaut on peut pull le repository par défaut d'opam, mais on
anticipe l'option qu'il va vouloir cloner le dépôt en local, faire des
bidouilles pour corriger des trucs, et tester ça. (Il faut que ça
marche pour des dépôts locaux, donc.)

Gabriel: nous on mantient un meilleur repository sur le côté avec des
fixes ?

Armaël: ça c'est indépendant.

Armaël: du coup il faut gérer les updates dans le repo. En fait il
y a moyen d'avoir un stamp pour l'état du repo, et on veut s'en servir
pour invalider la couverture qu'on aurait cachée.

Gabriel: pourquoi pas le commit git ?

Armaël: ce n'est pas forcément dans git. OPAM le gère en toute
généralité.

Gabriel: ah bon ? ça devrait être dans git de toute façon.

Gabriel: ça veut dire qu'il faut qu'on bosse sur la transformation des
erreurs en fix au dépôt. Aujourd'hui on travaille sur les couvertures
toutes seules.

Armaël: non, l'outil il va chercher dans le répertoire la couverture
mise en cache. (Et il l'invalide si le dépôt a changé.)

Gabriel: moi ça me plaît pas trop cet état implicite, on devrait
pouvoir fournir la couverture comme une option.

Armaël: c'est purement de la mémoisation.

Gabriel: non, les performances de build font partie de l'observable de
notre outil.

<Débat sur savoir si la couverture est une entrée/sortie à part ou
fait partie de l'état implicite de l'outil. Pas de consensus, mais on
va le laisser implicite pour commencer -- on fait au plus simple.>

Armaël: il faut que la couverture soit stockée avec le stamp du dépôt.

Gabriel: attention, souvent l'opam-repo a été modifié sur un aspect,
mais pas sur les failures que tu as vues.

Gabriel: si avec la couverture on gardait le log des échecs qui ont
été utilisés pour calculer la couverture, on pourrait à partir d'un
diff du opam-repo invalider certains échecs et garder les autres.

Armaël: d'accord, dans un second temps. Pour l'instant on fait une
version bête.

Gabriel: on verra aussi les performances du recalcul de la couverture.

Armaël: les failures qui n'ont pas changé sont toujours dans le cache,
donc elles vont échouer immédiatement.

Gabriel: en fait on pense à la mise à jour de la couverture comme un
processus dynamique, mais on peut le voir statiquement. Une couverture
c'est une paire de (1) un dépôt (2) une liste (ordonnée) d'échecs. La
mise à jour dynamique ajoute un nouvel échec à la liste et recalcule
(statiquement). Avec ce découpage de l'information on peut faire des
choses plus fines.

Armaël: super super, mais en deuxième itération.

----(circle)

Armaël: il faut savoir aussi comment on reporte les erreurs et les
non-erreurs. Un truc qu'on ne peut pas faire en V1, c'est les reporter
au fur et à mesure qu'ils arrivent. Ils sont printés à l'écran, mais
la fonction ne donne les résultats qu'à la fin. Tant pis.

Gabriel: ces trucs prennent du temps donc il faut garder une sortie
interactive au fil de l'eau, sinon on ne sait pas si ça marche ou
c'est bloqué.

Armaël: oui oui, opam-libs print ce qui se passe. Nous on ne peut pas
s'en servir dans nos outils, c'est tout.

Gabriel: est-ce que le binaire il tourne en permanence ou il s'arrête
après un run ?

Armaël: il s'arrête après un run.

Gabriel: un run avec recalcul de couvertures ou juste un run et on met
l'état implicite à jour ? On continue à builder après l'update ?

Armaël: un run jusqu'à la fin, on builde tout ce qu'on a pu builder
y-compris en patchant la couverture.

Gabriel: et si ça ne termine pas ?

Armaël: ça termine forcément. Quand on a des paquets qui ne buildent
pas, on n'essaie pas de les réparer, on builde le reste, à la fin on
n'a plus rien qui builde donc on s'arrête forcément. À chaque étape on
a moins de paquets qu'avant. Tu as enlevé ceux qui ont bien buildé et
ceux qui ont échoué.


Armaël: feature qu'il serait bien d'avoir, au moins de manière
semi-hardcodée dans la V1, c'est de pouvoir donner filtrer sur les
paquets qu'on veut tester.

Gabriel: quel langage de filtre ?

Armaël: pour l'instant juste quelques choix hardcodés. Les trucs importants:
- tous les paquets installables sur ma version du compilateur
  (Gabriel: quand on lance d'outil on spécifie une version du comilateur ?)
  (Armaël: oui. On ne fait pas de différentiel automatique pour l'instant.)
- toutes les revdeps du paquet X.
- tous les paquets qui sont dans une certaine liste. Cette liste doit
  pouvoir être feed l'output d'un run précédent. Comme ça on peut
  faire le différentiel à la main.

Gabriel: ok mais le différentiel alors te dira quels paquets ne
buildent plus, mais ne peux pas te dire quels paquets se mettent
à builder. Armaël: ça on s'en fout.

Gabriel: o-m-p on pourrait avoir une dev-release qui ne gère que
trunk, et on veut vérifier que ça gère bien trunk.

Armaël: dans ce cas tu l'ajoutes à la main dans la liste en plus du
différentiel.

Gabriel: sachant que dans le cas de trunk, on peut espérer que peu de
paquets sont marqués comme ne marchant que sur trunk, donc l'ensemble
de "trucs à essayer de nouveau" sera petit. (Les paquets
"intéressants" qui ne seront pas dans le run de 4.07.1, "omp dev
version", etc., peuvent être marqués comme ça dans opam et se comptent
sur les doigts d'une main.)

Ça suggère une autre liste par défaut:
- les paquets qui sont supportés par ma version du compilo mais pas
  les précédentes

(On marque omp-dev, merlin-dev comme ça dans l'opam-repository:
"depends ocaml >= dev-version".)

Armaël: tant que j'y pense, une feature qu'on ne mettra pas dans la
V1, la possibilité de facilement dériver des versions du compilo
à partir de branches, même pas dans le opam-repository.

Gabriel: sinon on n'est pas obligé de prendre le compilo dans
l'opam-repository, on peut donner un chemin local avec un ocaml.opam
dedans.

----

Armaël: si on relance des binaires avec une version qui existe déjà,
et on a déjà cette version du compilateur dans le OPAM
utilisateur... Tu initialises ton dossier avec 4.07.1, tu rappelles le
binaire avec une autre version du compilateur, qu'est-ce qu'il fait ?

Gabriel: oui on veut que ça marche bien, ça fait un switch de plus
dans le OPAMROOT local. Si la version est exactement la même, on
réutilise le switch existant. (Mais on le vire.)

Armaël: donc l'utilisateur n'a pas plusieurs switches avec le même
compilateur.

Gabriel: on parle du nom de version là, ou du nom de switch OPAM ?

Armaël: le nom du switch opam.

Armaël: on va avoir un problème avec plusieurs switches dans le même cache ?

Gabriel: non, le buildid il contient le hash de ocaml.

Armaël: Et si tu redonnes une URL de repo qui n'est pas la même ?

Ça on veut que ça donne une erreur.

----

On refait un essai de décrire l'interface de la V1 exhaustivement.

Vocabulaire:

- workdir: le répertoire de travail

- repo timestamp: one instant in opam-repository life (git hash, etc.)

- cover state: the state of the global cover, which evolves during
  a single repo timestamp

- cover element build: the build of one set of coinstallable packages
  (cover elements); upon partial failure, evolves the cover state

- binary cache: lives over the whole repo life, across repo
  timestamps, as the build id remains valid over timestamps. Caches
  both successes and failures. Needs to be garbage-collected from time
  to time -- and download failures can be retried, see below.

Prend en entrée:
- un répertoire de travail
- un opam-repository (obligatoire, un repo local)
- un nom de switch de compilateur OCaml (à chercher dans le opam-repository)
- une sélection de paquets à tester (en option, par défaut tout)
  avec un petit nombre de noms de sélections hardcodées

Il faut inclure une UI qui indique que ce répertoire de travail n'est
pas relocatable. Ça casse le cache.

Le binaire n'écrit jamais en dehors de ce répertoire de travail

Structure du répertoire de travail:
- un dossier `opamroot/`
  (survit sur tous les repo timestamps)

- un dossier `cache/` pour le cache binaire
  (on ne le met pas dans .cache, et pas dans opamroot/)
  (pourquoi pas dans opamroot/? on peut trasher le opamroot et réutiliser le cache)
  (survit sur tous les repo timestamps)

- un fichier `switches/<compiler-variant>/log` de tout ce qui s'est passé pour
  un switch donné avec la date+heure (survit sur tous les repo
  timestamps)

- un gitrepo switches/<compiler-variant>/current-timestamp.git qui contient les
  données timestamp-spécifique, versionnées à travers les cover element
  builds:
  - un commit par cover element build
  - des messages de commit qui indiquent les évolutions de la
    couverture

  Contenu:
  - le repo timestamp (ne change jamais) dans un fichier `timestamp`
  - `cover.json`, le cover state, avec les cover elements numérotés
  - `report.json`, contient les logs de succès et échec (les détails des erreurs)
    append-only, on ajoute des informations à chaque cover element build
  - `cover-element-build.id`, l'ID du build en cours

- un dossier switches/<compiler-variant>/past-timestamps/, qui contient
  les précédents $(write-date)-timestamp.git, on ne s'en sert jamais
  et l'utilisateur peut les flusher quand il veut

Sortie interactive:
- random stuff printed by the opam-libs
- les actions que nous on fait sur le répertoire de travail
  (en particulier le log et le report)

Sortie:
- résumé texte du report
- "lancez la commande ... pour générer le report HTML"

À chaque passage à un nouvel élément de la couverture on vire le
contenu du opamroot-switch (mais on garde les paquets dans le cache).

La commande de génération de HTML à partir du report.json est séparée
pour qu'on puise la lancer au fur et à mesure.

Armaël: Après il nous faut un page web qui sait visionner le JSON.

Gabriel: non, un build statique .JSON -> gros .HTML.

Armaël: mais du coup les progrès du build ne vont pas refresh la page
automatiquement. C'est une V1. De toute façon l'API ne nous donne pas
les infos au fur et à mesure.

## Atomicité

Au tout début (avant le premier cover-element build) on fait un
premier commit avec un report.json vide, cover.json statique et
cover-element-build.id égal à 1.

Quand on termine un cover-element build, on fait un commit dans
current-timestamp.git:
- report.json mis à jour
- cover.json évolué avec les nouveaux cover elements (attention à l'ordre des IDs)
- dans cover-element-build.id, le nouvel id à builder ensuite

Invariant: à tout moment cover-element-build.id est l'élément qu'on
est en train de builder (et à relancer si interruption). Le
report.json a des infos du cover-element courant, mais n'est pas
encore commité.

Gabriel: est-ce qu'on veut cleaner le cache des download failures de
temps en temps ?

Le build-id évolue si l'URL ou le checksum change. Le seul cas de
failure à retenter est un 404 sans changement d'archive.

On pourrait avoir une commande gc-like qui prend le report et
travaille juste sur les download-errors, et vire ce qui réussit du
cache des failures.

Armaël: donc il nous faut un deuxième binaire dédié pour la gestion du
cache.

Gabriel: ou des sous-commandes, c'est plus cool.

Gabriel: pour que le cache soit plus lisible, on voudrait avoir
paquet+version dans le nom de répertoire en plus du build-id.

Armaël: vendu.

Armaël: et le cache LRU dans l'outil de GC.

Gabriel: attention le report n'est valide que pendant un repo
timestamp, donc on ne pourra pas l'utiliser pour le LRU
across-timestamps. Je propose que notre script de "restore" aille
écrire la date courante dans le cache quand il restore un truc, pour
avoir facilement une vue globale de ce qui a servi récemment. (Donc
les données à restorer doivent être dans un sous-dossier pour pouvoir
stocker des informations en plus sur le côté.)


Armaël: il y a un truc qui nous manque, dans l'outil de gestion du
cache, il faut une option qui prend une liste de paquets-versions et
qui clear le cache pour ces trucs-là, notamment le cache des
failures. Et un truc qui clear le cache de toutes les failures.

Un truc qui nous manque c'est la gestion des depexts. On n'en a pas
parlé.

Gabriel: une sous-commande qui fait ça, qui installe toutes les
depexts.

Armaël: pour la cover courante, pour tous les paquets ? Mon workflow
c'est de builder sans depext, et si je vois une failure sur un paquet
auquel je tiens, je veux clearer le cache, installer la depext et
relancer le build.

Armaël: la commande depext prend sans doute en argument la même
sélection de paquet que tu as donnés à la commande principal.

Louis: OPAM il est capable de te donner la liste des paquets systèmes
dont il a besoin en fonction d'un set de paquets. C'est dans OPAM
directement, pas besoin du plugin. La seule chose que fait le plugin,
il est capable de dire "la commande pour installer les depexts c'est
...". Il exécute le sudo apt-install machin.

Armaël: donc en première approche on veut juste récupérer la liste des
paquets. Dans l'API ?

Louis: regarde comment c'est fait dans "opam list".

Raja: OpamListCommand.get_depexts. OpamSwitchState.depexts (prend un
paquet et renvoie les depexts; on fait un gros concat ensuite).


Gabriel: dans des cas assez courant, le opam-repository évolue plus
vite que nos builds. Est-ce que ça se passe bien si on interromp un
repo timestamp et on veut passer à la suite ? Control-C.

Armaël: deux situations, soit on fait Control-C et on relance ensuite,
sans mettre à jour le opam-repository. C'est le plus dur. Le cache va
être correct mais il faut recommencer le dernier cover element build.

Gabriel: est-ce qu'on trashe le switch courant avant de relancer le
cover element?

Armaël: dans quel état est le report.json ?

Gabriel: report.json, on ne peut pas le supprimer et relancer le
build, et le cache va le reconstruire rapidement ?

Armaël: on perd les cover elements précédents.

Gabriel: attend, on a current-timestamp.git.

Gabriel: c'est jouable tant qu'on écrit atomiquement les trucs à la
fin d'un cover element build, et qu'on s'assure qu'interrompre pendant
le cover-element-builder et le recommencer à partir du
current-timestamp.git (si une partie du boulot a déjà été fait et
interrompu) va bien refaire tout le boulot proprement.

- on fait un git reset --hard pour retourner à l'état du cover-elment précédent
- on trashe le switch
- on recommence le cover-element build


----

Raja propose d'intégrer le repro-testing à Marracheck. Résumé de notre idée:

- on lance une instance de marrachek dans un répertoire donné,
  avec BUILD_PATH_PREFIX_MAP qui va bien, et on attend que ça termine.

- on récupère la couverture finale pour relancer dans une deuxième
  instance séparée, avec le BUILD_PATH_PREFIX_MAP qui va bien,
  et on attend que ça termine.

- on compare deux-à-deux les Changes de chaque paquet des covers des
  deux instances.

Pour que ça marche bien il faut qu'on stocke les Changes de chaque
paquet buildé dans le report.

Après on a une sortie web où on montre la reproductibilité. L'outil de
Raja reste indispensable pour les mainteneurs de paquets incriminés
qui ne veulent pas tout builder (deux fois). Il faut qu'on leur
fournisse un lockfile pour chaque erreur de reproductibilité, et il
faudrait que l'outil de Raja sache manger un lockfile en entrée.

Pour que ça marche bien il faut qu'on stocke les lockfiles de chaque
paquet buildé dans le report. Le lockfile, c'est une explication du
build-id.

----

Armaël: Louis, comment on récupère un stamp à partir d'un
opam-repository ?

Louis: oui, dans switch-state tu peux récupérer repository-state avec
tes repository, dans le type `repos_state` tu peux trouver
`repos_definitions` OpamFile.stamp.... Ah non, si le opam-repo
n'indique pas lui-même le champ "stamp" dans sa config, on n'a pas
l'info, il faut appeler `revision` dans OpamRepository.

----

Armaël: il faut rajouter une sous-command "update" pour mettre à jour
le repo local de la OPAMROOT par rapport au repo donné par les gens.

Gabriel: si jamais ça auto-update, on n'a qu'à demander aux gens de
nous filer un path vers un clone local, comme ça ils contrôlent le
moment de l'update avec un "git pull". Mais ok, le mieux est de ne pas
auto-update.

Gabriel: Il faut aussi une commande pour avoir le hash du dépôt, pour
les gens qui veulent savoir s'ils doivent update ou pas.

<discussion sur les updates de timestamp>

Consensus: on va forcer les gens à travailler sur clone local du
opam-repository, comme ça ils gèrent eux les updates.

Gabriel: Quand on lance l'outil, on se met à jour par rapport au clone
local automatiquement ? Et on trashe les cover.json s'il y a eu un
changement ?

Armaël: oui.

C'est un peu subtil:
- chaque switch a son propre current-timestamp.git/timestamp
  qui indique le timestamp du dernier build dans ce switch
- notre opamroot a aussi un timestamp qui est automatiquement synchronisé
  avec celui du dépôt opam-repository local donné à la création du workdir,
  et qui est donc plus en avance que celui des switches

Quand on refait un build dans un switch, si le timestamp global
a avancé, on commit la cover partielle et on recommence avec une
nouvelle cover.

---

Louis: La seule règle philosophique qui est vraiment forcée dans la
codebase OPAM, c'est qu'on ne peut pas faire `try with _`. Si on
autorisait ça, on ne pourrait jamais gérer Control-C
correctement. C'est documenté dans OpamStd.Exn.

Armaël: ah mais en fait non regarde là dans le scheduler de
opamSolution, tu droppes des Sys.Break.

Louis: bon, le scheduler ça ne compte pas. Ça me rappelle vaguement
quelque chose, je crois qu'il y avait une raison.
