Armaël est en train de terminer le travail pour intégrer les actions
de Fetch dans le graphe d'action, au lieu de les faire toutes au
début.

Gabriel a envoyé sa PR sur la désérialization JSON.

---

Armaël: il y aura un breaking change dans la pull
request. Actuellement si un Fetch est en dépendance d'un Remove, et
que le Fetch échoue, opam va afficher un warning mais quand même faire
le remove en mode dégradé (supprimer les fichiers qu'il a record). Je
ne pourrai pas le faire dans ma PR, si le fetch échoue, c'est une
dépendance dans le graphe d'action qui fait échouer ses dépendances
automatiquement (dont le Remove).

Gabriel: dans le graphe d'action on ne peut pas dire "fait ça, et si
ça échoue fait ça" ? Aujourd'hui le remove est hardcodé dans le
"install en cas d'échec".

Louis, Armaël: ça complexifie le graphe.

Armaël: ce que dit Louis c'est que de toute façon on a envie de
corriger les paquets dans opam-repository pour que tous ceux qui n'ont
pas besoin d'une règle de désinstallation spéciales n'aient pas de
règle de règle de désinstallation. On fait une grosse PR dans le
repository.

Gabriel: mais Louis n'a pas déjà fait ça dans la migration ? Le flag
light-uninstall.

Louis: le flag dit juste que tu n'as pas besoin de télécharger les
sources.

Louis, Thomas: "ocamlfind remove" il peut rajouter des lignes dans un
fichier. Lui on ne peut pas le supprimer.

Armaël: du coup pour détecter quels paquets on veut simplifier
automatiquement, on pourrait après avoir installé tous les paquets
avec notre outil, regarder les fichiers Changes qui sont installés par
opam, regarder ce qui a été installé et modifié. On peut supposer
qu'un paquet qui ne fait qu'ajouter des fichiers peut être désinstallé
sans rien faire.

Gabriel: comment est-ce qu'on teste ça ? On fait cette grosse PR et on
vérifie qu'on ne s'est pas merdés.

Louis: ce que fait opam, il va lancer tes instructions, et ensuite lui
il regarde ce qu'il avait noté comme CHANGES, et il regarde ce qu'il
en reste. Il pourrait dumper une erreur dans un log s'il trouve qu'il
reste des trucs.

Gabriel: puis-je utiliser bubblewrap pour imposer de ne faire que des
écritures (suppressions) dans les fichiers du CHANGES, dans la règle
du remove ?

Louis: on a avait aussi discuté de mettre plusieurs pools de processus
dans le scheduler. On a déjà un pool différent pour les install, mais
il est limité à 1 donc c'est codé plus simplement.

----

Armaël: par ailleurs, Drup me signale l'existence d'un paquet de
Jérôme Vouillon et Roberto Di Cosmo qui parle de co-installabilité de
paquets debian. Ils cherchent des co-installability kernels en faisant
des réductions sur le graphe.

----

Armaël: moi j'ai soumis ma Pull-Request. Ils ont du backlog à reviewer
donc ils ne vont pas la reviewer tout de suite. Il y a quand même ce
petit problème agaçant du Fetch dans les Remove.

Gabriel: du coup, pourquoi ne pas hardcoder un `remove` automatique
quand Fetch échoue, comme ils font pour install?

Armaël: si tu as un pin pourri qui a une URL fausse, tu ne pourras
jamais le résinstaller.

Louis: oui, en pratique ça empêche de supprimer des paquets dans le
réseau.

Gabriel: différent Fetch, Fetch et Fetch-remove.

Gabriel: le Fetch il a un bit d'information, par défaut false. Avant
d'exécuter le graphe, on traverse le graphe pour observer tous les
nœuds Fetch et on met leur bit d'information correctement. L'action de
Remove regarde le bit pour décider si light-uninstall ou pas.

Armaël: si tu dépends d'au moins un truc qui n'est pas remove, on le
fait échouer, si il ne dépend que de Remove on silence l'échec pour
que Remove s'exécute.

Louis: en fait tu peux mettre cette annotation pendant la construction
du graphe, quand tu rajoutes les nœuds de Fetch.

Armaël: et quand je suis sur un nœud Remove, il faut que je teste si
un Fetch a eu lieu avant ou pas.

Louis: le scheduler te donne accès au résultat de tes antécédents, il
faut voir comment c'est fait dans ParalelApply.

Armaël, Gabriel: en fait, si un nœud Fetch n'a qu'un Remove comme
parent, on peut enlèver le Fetch.

Louis: oui mais le scheduling du téléchargement est moins bon.

Louis: le scheduler au moment où tu échoues, il va tuer tous ses fils,
mais tu peux juste envoyer une valeur de retour. Donc en fait tu
pourrais continuer à exécuter le Remove au lieu de lever une
exception. Fetch renvoie un booléen qui dit s'il a réussi ou pas. Si
tu essaies de faire un Build, tu échoues, mais si tu essaies de faire
un Remove, là tu réussis.

Gabriel: mais attention parce que si le Fetch a à la fois Build et
Remove comme parents, il va passer sa sortie aux deux, le Build va se
faire mais pas le Remove.

Louis: en fait non parce que dans ce cas on met le Build en dépendance
du Remove aussi. Donc c'est bon !

Louis, Armaël regardent le code. En fait c'est super facile !

----

Parallel_apply, l'ActionGraph qu'il prend en entrée, c'est avant
l'expansion des Fetch.

(Raja: Gabriel, ajoute une dépendance sur 'afl', le paquet OPAM pour
afl-fuzz.)

Gabriel: pour l'instant ta PR ne gère pas le fait d'avoir deux files
de tâches séparées pour les Fetch ?

Armaël: non, il veut le regarder lui.

